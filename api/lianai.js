import {
	request
} from '../utils/request.js'
import {
	appConfig
} from '../config/config.js'


// 教学详情
export function getTeachingDetail(params) {
	return request.post(appConfig.LIANAI_API + '/app/api/getTeachingDetail', params)
}

//获取话术详情
export function getWordDetails(params) {
	return request.post(appConfig.LIANAI_API + '/app/api/getWordDetails', params)
}

//获取首页banner列表
export function getBannerList(params) {
	return request.get(appConfig.LIANAI_API + '/app/api/getBannerList', params)
}

//获取首页公告列表
export function getAnnounList(params) {
	return request.get(appConfig.LIANAI_API + '/app/api/getAnnounList', params)
}

//获取热门话术分类
export function getDictionType(params) {
	return request.get(appConfig.LIANAI_API + '/app/api/getDictionType', params)
}

//获取文章列表
export function getArticleIndex(params) {
	return request.post(appConfig.LIANAI_API + '/app/api/getArticleIndex', params)
}

//获取话语列表
export function getWordList(params) {
	return request.get(appConfig.LIANAI_API + '/app/api/getWordList', params)
}
