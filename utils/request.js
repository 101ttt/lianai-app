import {
	appConfig
} from '../config/config.js'
import {
	tokenUtil
} from './token'
import {
	Base64
} from './base64.js'

const send = (url, data = {}, method = 'POST', showLoading = false) => {
	return new Promise((resolve) => {
		uni.request({
			method: method,
			url: url,
			data: data,
			// header: (() => {
			// 	const tokeValue = tokenUtil.get()
			// 	let config = {
			// 		// 'Content-Type': 'application/x-www-form-urlencoded'
			// 		'User-Type': 'webapp',
			// 		'Content-Type': 'application/json;charset=UTF-8',
			// 		'Authorization':'Basic d2ViYXBwOndlYmFwcF8xMTIzNTk4ODExNzM4Njc1MzMz'
			// 	}
			// 	if (tokeValue) {
			// 		config[appConfig.tokenKey] = tokeValue
			// 	}
			// 	return config
			// })(),
			success: (res) => {
				console.log(res)
				if (res.data.data == "token令牌未被识别") {
					uni.showToast({
						icon: "none",
						title: "登录失效，请重新登录！",
					})
					setTimeout(function() {
						uni.navigateTo({
							url: '/pages/user/login',
						});
					}, 500);
				} else {
					resolve(res.data)
				}

			}
		})
	})
}

export const request = {
	get: (url, data) => {
		return send(url, data, 'GET')
	},
	post: (url, data) => {
		return send(url, data, 'POST')
	}
}
