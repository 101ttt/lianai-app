import Vue from 'vue'
import App from './App'

//tabBar组件
import tabBar from '@/pages/index/tabbar.vue'
Vue.component('tabBar', tabBar)
// 此处为引用自定义顶部
// 引入:uView-UI
import uView from 'uview-ui';
Vue.use(uView);

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
